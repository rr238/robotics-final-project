# Jacob Manders
# Setting up the chessboard and logic for the final project

import numpy
import random
import copy

# Here's how we'll keep track: first digit will indicate if it's white (1) or black (2) and second will indicate the piece type
# pawn (1) rook (2) knight (3) bishop (4) king (5) queen (6)
# so example is white rook is 12, black queen is 25

#initial config - make a function for this reset?
def reset_board():
    board = numpy.zeros((8,8), dtype=int)
    board[0] = [22, 23, 24, 26, 25, 24, 23, 22]
    board[1] = [21, 21, 21, 21, 21, 21, 21, 21]

    board[6] = [11, 11, 11, 11, 11, 11, 11, 11]
    board[7] = [12, 13, 14, 16, 15, 14, 13, 12]
    return board

def is_my_piece(board, square, isWhiteTurn):
    if (board[square[0]][square[1]] == 0):
        # print("No piece in indicated position")
        return 0
    if ((board[square[0]][square[1]] // 10 == 1) != isWhiteTurn):
        # print("Piece in indicated square is not mine")
        return -1
    if ((board[square[0]][square[1]] // 10 == 1) == isWhiteTurn):
        return 1
    else:
        print("somethings broken - is_my_piece")
        return -2

# return legal moves for a pawn in a given board configuration
def pawn_moves(board, position, isWhiteTurn):
    moves = [];
    if (isWhiteTurn):
        blocked =  (position[0] == 0) or (is_my_piece(board, [position[0]-1, position[1]], isWhiteTurn) != 0)
        if (not blocked):
            moves.append([position[0]-1, position[1]]) # add square in front if not blocked
        if( not blocked and position[0] == 6): # if not blocked and in starting position, add pos 2 in front
            if (is_my_piece(board, [4, position[1]], isWhiteTurn) == 0):
                moves.append([4, position[1]])
        #check attacking positions
        if ((position[0] != 0) and (position[1] != 0) and is_my_piece(board, [position[0] - 1, position[1]-1], isWhiteTurn) == -1):
            moves.append([position[0] - 1, position[1] - 1])
        if ((position[0] != 0) and (position[1] != 7) and is_my_piece(board, [position[0] - 1, position[1]+1], isWhiteTurn) == -1):
            moves.append([position[0] - 1, position[1] + 1])
    else: #black turn
        blocked =  (position[0] == 7) or (is_my_piece(board, [position[0]+1, position[1]], isWhiteTurn) != 0)
        if (not blocked):
            moves.append([position[0]+1, position[1]]) # add square in front if not blocked
        if( not blocked and position[0] == 1): # if not blocked and in starting position, add pos 2 in front
            if (is_my_piece(board, [3, position[1]], isWhiteTurn) == 0):
                moves.append([3, position[1]])
        #check attacking positions
        if ((position[0] != 7) and (position[1] != 0) and is_my_piece(board, [position[0] + 1, position[1]-1], isWhiteTurn) == -1):
            moves.append([position[0] + 1, position[1] - 1])
        if ((position[0] != 7) and (position[1] != 7) and is_my_piece(board, [position[0] + 1, position[1]+1], isWhiteTurn) == -1):
            moves.append([position[0] + 1, position[1] + 1])
    return moves

#return legal moves for a rook in a given board configuration
def rook_moves(board, position, isWhiteTurn):
    moves = []
    #check all 4 directions
    #up
    i = position[0]
    while i > 0:
        if (board[i-1][position[1]] == 0): # open square
            moves.append([i-1, position[1]])
            i -= 1
        elif (is_my_piece(board, [i-1, position[1]], isWhiteTurn) == -1): #capturable piece
            moves.append([i-1, position[1]])
            break
        else: # your own piece
            break

    #down
    i = position[0]
    while i < 7:
        if (board[i+1][position[1]] == 0): # open square
            moves.append([i+1, position[1]])
            i += 1
        elif (is_my_piece(board, [i+1, position[1]], isWhiteTurn) == -1): #capturable piece
            moves.append([i+1, position[1]])
            break
        else: # your own piece
            break
    
    #Left
    i = position[1]
    while i > 0:
        if (board[position[0]][i-1] == 0): # open square
            moves.append([position[0], i-1])
            i -= 1
        elif (is_my_piece(board, [position[0], i-1], isWhiteTurn) == -1): #capturable piece
            moves.append([position[0], i-1])
            break
        else: # your own piece
            break

    #right
    i = position[1]
    while i < 7:
        if (board[position[0]][i+1] == 0): # open square
            moves.append([position[0], i+1])
            i += 1
        elif (is_my_piece(board, [position[0], i+1], isWhiteTurn) == -1): #capturable piece
            moves.append([position[0], i+1])
            break
        else: # your own piece
            break          
        
    return moves
    
#return legal moves for a knight in a given board configuration
def knight_moves(board, position, isWhiteTurn):
    moves = []
    row = position[0]
    col = position[1]
    # have to check all 8 possibilities, not sure there's a better way to do this
    
    #down and right moves first
    if(row+2 <= 7 and col+1 <=7 and is_my_piece(board, [row+2, col+1], isWhiteTurn) != 1):
        moves.append([row+2, col+1])
    if(row+1 <= 7 and col+2 <=7 and is_my_piece(board, [row+1, col+2], isWhiteTurn) != 1):
        moves.append([row+1, col+2])
        
    #down and left moves
    if(row+2 <= 7 and col-1 >=0 and is_my_piece(board, [row+2, col-1], isWhiteTurn) != 1):
       moves.append([row+2, col-1])
    if(row+1 <= 7 and col-2 >=0 and is_my_piece(board, [row+1, col-2], isWhiteTurn) != 1):
       moves.append([row+1, col-2])
       
    # up and left moves
    if(row-2 >= 0 and col-1 >=0 and is_my_piece(board, [row-2, col-1], isWhiteTurn) != 1):
       moves.append([row-2, col-1])
    if(row-1 >= 0 and col-2 >=0 and is_my_piece(board, [row-1, col-2], isWhiteTurn) != 1):
       moves.append([row-1, col-2])
       
    #up and right moves
    if(row-2 >= 0 and col+1 <= 7 and is_my_piece(board, [row-2, col+1], isWhiteTurn) != 1):
       moves.append([row-2, col+1])
    if(row-1 >= 0 and col+2 <=7 and is_my_piece(board, [row-1, col+2], isWhiteTurn) != 1):
       moves.append([row-1, col+2])
    
    return moves

# return legal moves for a bishop in a given board configuration
def bishop_moves(board, position, isWhiteTurn):
    moves =[]
    #check all 4 directions
    #up left
    i = position[0]
    j = position[1]
    while i > 0 and j > 0:
        if (board[i-1][j-1] == 0): # open square
            moves.append([i-1, j-1])
            i -= 1
            j -= 1
        elif (is_my_piece(board, [i-1, j-1], isWhiteTurn) == -1): #capturable piece
            moves.append([i-1, j-1])
            break
        else: # your own piece
            break

    #up right
    i = position[0]
    j = position[1]
    while i > 0 and j < 7:
        if (board[i-1][j+1] == 0): # open square
            moves.append([i-1, j+1])
            i -= 1
            j += 1
        elif (is_my_piece(board, [i-1, j+1], isWhiteTurn) == -1): #capturable piece
            moves.append([i-1, j+1])
            break
        else: # your own piece
            break
    
    #down left
    i = position[0]
    j = position[1]
    while i < 7 and j > 0:
        if (board[i+1][j-1] == 0): # open square
            moves.append([i+1, j-1])
            i += 1
            j -= 1
        elif (is_my_piece(board, [i+1, j-1], isWhiteTurn) == -1): #capturable piece
            moves.append([i+1, j-1])
            break
        else: # your own piece
            break

    #down right
    i = position[0]
    j = position[1]
    while i < 7 and j < 7:
         if (board[i+1][j+1] == 0): # open square
             moves.append([i+1, j+1])
             i += 1
             j += 1
         elif (is_my_piece(board, [i+1, j+1], isWhiteTurn) == -1): #capturable piece
             moves.append([i+1, j+1])
             break
         else: # your own piece
             break
         
    return moves

# return legal moves for a queen in a given board configuration
def queen_moves(board, position, isWhiteTurn):
    # queen moves are just anything a rook or bishop can do
    moves = rook_moves(board, position, isWhiteTurn)
    bmoves = bishop_moves(board, position, isWhiteTurn)
    if (len(bmoves) != 0):
        for x in bmoves:
            moves.append(x)
    return moves

#helper for check algorithm - checks diagonals
def check_diagonals(board, position, isWhiteTurn):
    #check all 4 directions
    #up left
    i = position[0]
    j = position[1]
    while i > 0 and j > 0:
        piece = board[i-1][j-1]
        isWhite = piece //10 == 1
        ptype = piece % 10
        if (piece == 0): # open square
            i -= 1
            j -= 1
        elif (isWhite) != isWhiteTurn and (ptype == 4 or ptype ==6): #attacking piece
            return True
        else: # your own piece or non-attacking piece (or maybe pawn - we will check)
            break

    #up right
    i = position[0]
    j = position[1]
    while i > 0 and j < 7:
        piece = board[i-1][j+1]
        isWhite = piece //10 == 1
        ptype = piece % 10
        if (piece == 0): # open square
            i -= 1
            j += 1
        elif (isWhite) != isWhiteTurn and (ptype == 4 or ptype ==6): #attacking piece
            return True
        else: # your own piece or non-attacking piece (or maybe pawn - we will check)
            break
    
    #down left
    i = position[0]
    j = position[1]
    while i < 7 and j > 0:
        piece = board[i+1][j-1]
        isWhite = piece //10 == 1
        ptype = piece % 10
        if (piece == 0): # open square
            i += 1
            j -= 1
        elif (isWhite != isWhiteTurn) and (ptype == 4 or ptype ==6): #attacking piece
            return True
        else: # your own piece or non-attacking piece (or maybe pawn - we will check)
            break

    #down right
    i = position[0]
    j = position[1]
    while i < 7 and j < 7:
        piece = board[i+1][j+1]
        isWhite = piece //10 == 1
        ptype = piece % 10
        if (piece == 0): # open square
            i += 1
            j += 1
        elif (isWhite != isWhiteTurn) and (ptype == 4 or ptype ==6): #attacking piece
            return True
        else: # your own piece or non-attacking piece (or maybe pawn - we will check)
            break
         
    return False

#helper for check algorithm - checks horizontals and verticals
def check_rcs(board, position, isWhiteTurn):
    #check all 4 directions
    #up
    i = position[0]
    j = position[1]
    while i > 0:
        piece = board[i-1][j]
        isWhite = piece //10 == 1
        ptype = piece % 10
        if (piece == 0): # open square
            i -= 1
        elif (isWhite) != isWhiteTurn and (ptype == 2 or ptype ==6): #attacking piece
            return True
        else: # your own piece or non-attacking piece (or maybe pawn - we will check)
            break

    #down
    i = position[0]
    while i < 7:
        piece = board[i+1][j]
        isWhite = piece //10 == 1
        ptype = piece % 10
        if (piece == 0): # open square
            i += 1
        elif (isWhite) != isWhiteTurn and (ptype == 2 or ptype ==6): #attacking piece
            return True
        else: # your own piece or non-attacking piece (or maybe pawn - we will check)
            break
    
    #Left
    i = position[0]
    j = position[1]
    while j > 0:
        piece = board[i][j-1]
        isWhite = piece //10 == 1
        ptype = piece % 10
        if (piece == 0): # open square
            j -= 1
        elif (isWhite) != isWhiteTurn and (ptype == 2 or ptype ==6): #attacking piece
            return True
        else: # your own piece or non-attacking piece (or maybe pawn - we will check)
            break

    #right
    i = position[0]
    j = position[1]
    while j < 7:
        piece = board[i][j+1]
        isWhite = piece //10 == 1
        ptype = piece % 10
        if (piece == 0): # open square
            j += 1
        elif (isWhite) != isWhiteTurn and (ptype == 2 or ptype ==6): #attacking piece
            return True
        else: # your own piece or non-attacking piece (or maybe pawn - we will check)
            break          
        
    return False
  
#helper for check algorithm - checks knight positions
def check_knights(board, position, isWhiteTurn):
    row = position[0]
    col = position[1]
    # have to check all 8 possibilities, not sure there's a better way to do this
    
    #down and right moves first
    if(row+2 <= 7 and col+1 <=7 and is_my_piece(board, [row+2, col+1], isWhiteTurn) == -1):
        if(board[row+2][col+1] % 10 == 3):
            return True
    if(row+1 <= 7 and col+2 <=7 and is_my_piece(board, [row+1, col+2], isWhiteTurn) == -1):
        if(board[row+1][col+2] % 10 == 3):
            return True
        
    #down and left moves
    if(row+2 <= 7 and col-1 >=0 and is_my_piece(board, [row+2, col-1], isWhiteTurn) == -1):
       if(board[row+2][col-1] % 10 == 3):
           return True
    if(row+1 <= 7 and col-2 >=0 and is_my_piece(board, [row+1, col-2], isWhiteTurn) == -1):
       if(board[row+1][col+-2] % 10 == 3):
           return True
       
    # up and left moves
    if(row-2 >= 0 and col-1 >=0 and is_my_piece(board, [row-2, col-1], isWhiteTurn) == -1):
       if(board[row-2][col-1] % 10 == 3):
           return True
    if(row-1 >= 0 and col-2 >=0 and is_my_piece(board, [row-1, col-2], isWhiteTurn) == -1):
       if(board[row-1][col-2] % 10 == 3):
           return True
       
    #up and right moves
    if(row-2 >= 0 and col+1 <= 7 and is_my_piece(board, [row-2, col+1], isWhiteTurn) == -1):
       if(board[row-2][col+1] % 10 == 3):
           return True
    if(row-1 >= 0 and col+2 <=7 and is_my_piece(board, [row-1, col+2], isWhiteTurn) == -1):
       if(board[row-1][col+2] % 10 == 3):
           return True
       
    return False

#helper for check algorithm - checks for attacking pawns
def check_pawns(board, position, isWhiteTurn):
    row = position[0]
    col = position[1]
    if isWhiteTurn: #check upwards if is white turn
        if (row>0 and col > 0 and board[row-1][col-1] == 21):
            return True
        if (row > 0 and col < 7 and board[row-1][col+1] == 21):
            return True
        
    else: #isblack turn, check downwards
        if (row<7 and col > 0 and board[row+1][col-1] == 11):
            return True
        if (row < 7 and col < 7 and board[row+1][col+1] == 11):
            return True
    
#determine if king is in check
def is_king_in_check(board, isWhiteTurn):
    if isWhiteTurn:
        whiteKing = numpy.argwhere(board == 15)
        whiteKing = [whiteKing[0][0], whiteKing[0][1]]
        king_pos = whiteKing
    else:
        blackKing = numpy.argwhere(board == 25)
        blackKing = [blackKing[0][0], blackKing[0][1]]
        king_pos = blackKing
    if(check_diagonals(board, king_pos, isWhiteTurn)):
        return True
    if(check_rcs(board, king_pos, isWhiteTurn)):
        return True
    if(check_knights(board, king_pos, isWhiteTurn)):
        return True
    if(check_pawns(board, king_pos, isWhiteTurn)):
        return True
    #check king - first find opposing king
    if isWhiteTurn:
        opposing_king = numpy.argwhere(board == 25)
        opposing_king = [opposing_king[0][0], opposing_king[0][1]]
    else:
        opposing_king = numpy.argwhere(board == 15)
        opposing_king = [opposing_king[0][0], opposing_king[0][1]]
    row_diff = abs(king_pos[0] - opposing_king[0])
    col_diff = abs(king_pos[1] - opposing_king[1])
    if(row_diff == 0 and col_diff == 0):
        print("messed up, checked same king")
    if row_diff <= 1 and col_diff <= 1:
        return True
    return False

# return legal moves for a king in a given board configuration
def king_moves(board, position, isWhiteTurn):
    moves = []
    vals = [-1, 0, 1]
    for i in vals:
        for j in vals:
            if (i==0 and j==0): # (current position)
                continue
            row = position[0] + i
            col = position[1] + j
            if (row>=0 and row<=7 and col>=0 and col<=7):
                if(is_my_piece(board, [row,col], isWhiteTurn) != 1):
                    #try_board = copy.copy(board)
                    #try_board[row][col] = board[position[0]][position[1]]
                    #try_board[position[0]][position[1]] = 0
                    #if not is_king_in_check(try_board, [row,col], isWhiteTurn):                           
                    moves.append([row,col])
    return moves

# represent isWhiteTurn as boolean, start and end as 1x2 array rep. Letter, # 
def available_moves(board, position, isWhiteTurn):
    # check if the indicated start square contains your piece
    if (is_my_piece(board, position, isWhiteTurn) == 0):
        print("No piece in inidcated position")
        return [];
    if (is_my_piece(board, position, isWhiteTurn) == -1):
        #print("Piece is indicated position is not yours")
        return [];
    if (is_my_piece(board, position, isWhiteTurn) != 1):
        # print("error: is_my_piece == -2")
        return [];
    
    #match piece type to available moves
    ptype = board[position[0]][position[1]] % 10
    moves = []
    if ptype == 1:
        moves =  pawn_moves(board, position, isWhiteTurn)
    elif ptype == 2:
        moves = rook_moves(board, position, isWhiteTurn)
    elif ptype == 3:
        moves = knight_moves(board, position, isWhiteTurn)
    elif ptype == 4:
        moves = bishop_moves(board, position, isWhiteTurn)
    elif ptype == 5:
        moves = king_moves(board, position, isWhiteTurn)
    elif ptype == 6:
        moves = queen_moves(board, position, isWhiteTurn)
    else:
        print('type not found: ' + ptype)
        return None
    
    """blackKing = numpy.argwhere(board == 25)
    blackKing = [blackKing[0][0], blackKing[0][1]]
    whiteKing = numpy.argwhere(board == 15)
    whiteKing = [whiteKing[0][0], whiteKing[0][1]]
    if isWhiteTurn:
        kingpos = whiteKing
    else:
        kingpos = blackKing"""
    i = 0
    while i < len(moves):
        x = moves[i]
        try_board = copy.copy(board)
        #print(x)
        try_board[x[0]][x[1]] = board[position[0]][position[1]]
        try_board[position[0]][position[1]] = 0
        if is_king_in_check(try_board, isWhiteTurn):
            del moves[i]
            continue
        i += 1
    return moves
        
#Evaluate checkmate 
def checkmate(board, isWhiteTurn):
    if not is_king_in_check(board, isWhiteTurn):
        return False
    if isWhiteTurn:
        color = 1
    else:
        color = 2
    for i in  range(8):
        for j in range(8):
            if (board[i][j] // 10 == color):
                piece_moves = available_moves(board, [i,j], isWhiteTurn)
                if (len(piece_moves) != 0):
                    print(str([i,j]) + " to " + str(piece_moves))
                    return False
    return True

## Testing    

#generate random board for testing 
def gen_random_board():
    board = numpy.zeros((8,8), dtype=int)
    pieces = [11, 11, 11, 11, 11, 11, 11, 11, 21, 21, 21, 21, 21, 21, 21, 21, 12, 12, 22, 22, 13, 13, 23, 23, 14, 14, 24, 24, 15, 16, 25, 26]
    i = 0
    j = 0
    while (len(pieces) > 0):
        if(board[i][j] == 0):
            if(random.randint(0,1) == 1):
                index = random.randint(0,len(pieces)-1)
                board[i][j] = pieces[index]
                del pieces[index]
        j = (j+1) % 8
        if(j == 0):
           # if(i == 7):    #uncomment if you don't want every piece on the board
               # break
            i = (i+1) % 8                
    return board

# test the is_my_piece() function
def test_is_my_piece():
    print("test_is_my_piece()")
    passed = True
    board = reset_board()
    if(is_my_piece(board, [4,1], True) != 0):
        print("test 1 failed")
        passed = False
    if(is_my_piece(board, [6, 5], True) != 1):
       print("test 2 failed")
       passed = False
    if(is_my_piece(board, [1, 2], True) != -1):
       print("test 3 failed")
       passed = False
    if(is_my_piece(board, [3,7], False) != 0):
        print("test 4 failed")
        passed = False
    if(is_my_piece(board, [0, 1], False) != 1):
        print("test 5 failed")
        passed = False
    if(is_my_piece(board, [7,4], False) != -1):
        print("test 6 failed")
        passed = False
    print("test_is_my_piece() passed: " + str(passed))
 
#test pawn_moves()
def test_pawn_moves():
    print("test_pawn_moves()")
    board = reset_board()
    board[3][4] = 11
    board[6][4] = 0
    x = available_moves(board, [3,4], True)
    print(x)
      

#test rook moves()
def test_rook_moves():
    board = gen_random_board()
    print(board)
    found = 0
    i = 0
    j = 0
    while (found < 4):
        if(board[i][j] % 10 == 2):
            found += 1
            print("rook at " + str(i) + "," + str(j))
            if(board[i][j] // 10 == 1):
                print(available_moves(board,[i,j], True))
            else: 
                print(available_moves(board, [i,j], False))
        j = (j+1) % 8
        if(j == 0):
            i = (i+1) % 8

# test knight moves
def test_knight_moves():
    board = gen_random_board()
    print(board)
    found = 0
    i = 0
    j = 0
    while (found < 4):
        if(board[i][j] % 10 == 3):
            found += 1
            print("knight at " + str(i) + "," + str(j))
            if(board[i][j] // 10 == 1):
                print(available_moves(board,[i,j], True))
            else: 
                print(available_moves(board, [i,j], False))
        j = (j+1) % 8
        if(j == 0):
            i = (i+1) % 8

#test bishop moves
def test_bishop_moves():
    board = gen_random_board()
    print(board)
    found = 0
    i = 0
    j = 0
    while (found < 4):
        if(board[i][j] % 10 == 4):
            found += 1
            print("bishop at " + str(i) + "," + str(j))
            if(board[i][j] // 10 == 1):
                print(available_moves(board,[i,j], True))
            else: 
                print(available_moves(board, [i,j], False))
        j = (j+1) % 8
        if(j == 0):
            i = (i+1) % 8

# test queen moves
def test_queen_moves():
    board = gen_random_board()
    print(board)
    found = 0
    i = 0
    j = 0
    while (found < 2):
        if(board[i][j] % 10 == 6):
            found += 1
            print("queen at " + str(i) + "," + str(j))
            if(board[i][j] // 10 == 1):
                print(available_moves(board,[i,j], True))
            else: 
                print(available_moves(board, [i,j], False))
        j = (j+1) % 8
        if(j == 0):
            i = (i+1) % 8     
            
# test king moves
def test_king_moves():
    board = gen_random_board()
    print(board)
    found = 0
    i = 0
    j = 0
    while (found < 2):
        if(board[i][j] % 10 == 5):
            found += 1
            print("king at " + str(i) + "," + str(j))
            if(board[i][j] // 10 == 1):
                print(available_moves(board,[i,j], True))
            else: 
                print(available_moves(board, [i,j], False))
        j = (j+1) % 8
        if(j == 0):
            i = (i+1) % 8

# test king in check algorithm
def test_is_king_in_check():
    board = gen_random_board()
    print(board)
    blackKing = numpy.argwhere(board == 25)
    blackKing = [blackKing[0][0], blackKing[0][1]]
    whiteKing = numpy.argwhere(board == 15)
    whiteKing = [whiteKing[0][0], whiteKing[0][1]]
    print("Black King in check: " + str(is_king_in_check(board,blackKing, False)))
    print("White King in check: " + str(is_king_in_check(board,whiteKing, True)))
  
#test checkmate detection algorithm
def test_checkmate():
    board = gen_random_board()
    """board[0] =[21, 11, 21, 21,  0,  0,  0,  0]
    board[1] =[26,  0, 22,  0,  0,  0, 11, 21]
    board[2] = [21,  0,  0, 23,  0, 14,  0,  0]
    board[3] = [ 0,  0, 11, 16, 23, 11,  0,  0]
    board[4] = [ 0, 25,  0, 12, 13, 11, 22,  0]
    board[5] = [ 0, 21,  0, 11,  0, 24, 15,  0]
    board[6] = [12,  0, 13,  0,  0,  0,  0,  0]
    board[7] = [11, 21,  0,  0, 21, 24, 11, 14]"""
    print(board)
    whiteCM = checkmate(board, True)
    blackCM = checkmate(board, False)
    print("White check: " + str(is_king_in_check(board, True)))
    print("White checkmate: " + str(whiteCM))
    #print(king_moves(board, whiteKing, True))
    #print(available_moves(board, whiteKing, True))
    print("Black check: " + str(is_king_in_check(board, False)))
    print("Black checkmate: " + str(blackCM))
    #print(king_moves(board, blackKing, False))
    #print(available_moves(board, blackKing, False))
      
# test_is_my_piece()
#test_pawn_moves()
# print(gen_random_board())
#test_rook_moves()
#test_knight_moves()
#test_bishop_moves()
#test_queen_moves()
#test_king_moves()
#test_is_king_in_check()
#test_checkmate()