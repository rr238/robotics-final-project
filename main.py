# Jacob Manders

# main driver for the chessboard 

import logic
from user_interface import UserInterface as UI
#import user_interface as UI
#import Chess_Setup.chess_setup as setup
#import MotionPlanning.PandaMove_Working_12_6 as mp
import PandaIntegrated as Panda

# execute the move - only to be used once the move has been checked to be valid
def execute_move(board, start, end):
    piece = board[start[0]][start[1]]
    board[end[0]][end[1]] = piece
    board[start[0]][start[1]] = 0
    return board

def main():
    #TODO
    
    """
    Here's the general flow:
        
    1. prompt user to enter square of piece to move (probably classic letter-number representation)
        let UI class decipher input and handle whether it is a valid input
        If valid, convert input to [row, column] of board
        If invalid, continue to prompt until valid input given
    2. Pass [r,c] to logic class to determine whether that user's piece is there
        If not, return to 1 to prompt for new input
        If a piece is there, determine whether there are any legal moves for that piece
        If not, inform user 'no legal moves' and return to 1 for new input
        Return list of legal moves in [r,c] format
    3. Pass list to robot geometry class
        Change color of squares to show available moves (maybe also the selected piece?)
    4. Pass list of available moves to UI class
        Convert to LETTER NUMBER representation for user
    5. Prompt user to select one of available moves
        UI class process input again
    6. Send move to robot class to move piece in Gazebo
        Change colors of available moves back to original board color 
        If capturing a piece, make sure to remove the captured piece from the scene before moving block (or else collision)
        Actually move the block
    7. Execute the move in the main class to update the board
    8. Update whose turn it is and repeat
    
    Things that we need but not sure where they fit in:
        a. an escape? Like if you select a piece but want to back up and select a different piece to move
        b. Evaluating check/checkmate
    """
    board = logic.reset_board()
    Panda.load_all()
    isWhiteTurn = True
    gameOver = False
    while (not gameOver):
        start = UI.promptUserForStartPos()
        if (start == -1):
            gameOver = True
            if isWhiteTurn:
                print("White has quit the game. Thank you for playing!")
                continue
            else:
                print("Black has quit the game. Thank you for playing!")
                continue
        moves = logic.available_moves(board, start, isWhiteTurn)
        if (len(moves) == 0):
            print("No available moves for this piece, select again")
            continue
        end = UI.promptUserForEndPos(moves) 
        if (end == -1):
            continue
        board = execute_move(board, start, end) 
        capturing = (logic.is_my_piece(board, end, isWhiteTurn))
        Panda.main(start, end, capturing)
        isWhiteTurn = not isWhiteTurn
        if (isWhiteTurn):
            color = "White"
        else:
            color = "Black"
        print(color + "'s turn!")
        if(logic.is_king_in_check(board, isWhiteTurn)): 
            print(color + " is in check")
            if (logic.checkmate(board, isWhiteTurn)):
                print(color + " is in checkmate")
                gameOver = True
                print("Game over - Thank you for playing!")
        
        
main()      