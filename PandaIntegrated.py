#!/usr/bin/env python
#****Note: The move-it python interface tutorial code was referenced and used for different portions of this code
from __future__ import print_function
from six.moves import input
#packages important to control the robot in Gazebo using MoveIt-Python interface
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from gazebo_msgs.srv import SpawnModel
from geometry_msgs.msg import Point, Pose, Quaternion
from franka_gripper.msg import GraspAction, GraspGoal, MoveGoal, MoveAction
import actionlib
from moveit_msgs.msg import RobotTrajectory
#from gazebo import Gripper

#import launch_files as lf 
#import GraspandRelease as gr
global zup
global zdown
zup = 0.5
zdown = 0.16
#condition that enables code to work with different versions of python based on what packages it can import
try:
    from math import pi, tau, dist, fabs, cos
except:
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

def convert_rowcol_to_xy(pos):
    python_row = pos[0]
    python_col = pos[1]
    
    board_size_in = 20
    board_size_m = board_size_in/39.37
    space_width = board_size_m/8

    board_center_x = 20/39.37
    board_center_y = 0/39.37
    
    gazebo_x = board_center_x + (python_col-3.5) * space_width
    gazebo_y = board_center_y + (3.5-python_row) * space_width
    
    return [gazebo_x, gazebo_y, 0.0254]

def all_close(goal, actual, tolerance):
    
    if type(goal) is list:
        # returns False if measurement is incorrect, i.e. if difference is above the desired tolerance
        for index in range(len(goal)):
            if abs(actual[index] - goal[index]) > tolerance:
                return False

    elif type(goal) is geometry_msgs.msg.PoseStamped:
        return all_close(goal.pose, actual.pose, tolerance)

    # establishes whether both the distance in space and the angle between the orientation measurement and the vertical at a specific pose goal is below the tolerance specified
    elif type(goal) is geometry_msgs.msg.Pose:
        x0, y0, z0, qx0, qy0, qz0, qw0 = pose_to_list(actual)
        x1, y1, z1, qx1, qy1, qz1, qw1 = pose_to_list(goal)
        d = dist((x1, y1, z1), (x0, y0, z0))

        cos_phi_half = fabs(qx0 * qx1 + qy0 * qy1 + qz0 * qz1 + qw0 * qw1)
        return d <= tolerance and cos_phi_half >= cos(tolerance / 2.0)

    return True

#the myProject class is created so that functions can be utilized to affect the robot's motion and position and space in the main class
class myProject(object):
    """myProject"""

    #this function initializes the relevant objects and nodes so MoveIt can communicate with the robot in the Gazebo simulation
    def __init__(self):
        super(myProject, self).__init__()

        #the moveit_commander is initialized so the code can use MoveIt to create joint states and pose goals in the following functions
        moveit_commander.roscpp_initialize(sys.argv)
        
        #the rospy node is initialized to allow for communication with ROS
        rospy.init_node("myProject", anonymous=True)

        #A moveit_commander object, called robot, is made
        robot = moveit_commander.RobotCommander()

        scene = moveit_commander.PlanningSceneInterface()

        #the "manipulator" group name allows the commander to use the arm joints in the UR5e robot specifically
        group_name = "panda_arm"
        move_group = moveit_commander.MoveGroupCommander(group_name)
        display_trajectory_publisher = rospy.Publisher("/move_group/display_planned_path", moveit_msgs.msg.DisplayTrajectory, queue_size=20,)
        
        hand_group_name = "panda_hand"
        move_group_hand = moveit_commander.MoveGroupCommander(hand_group_name)
        
        self.move_group = move_group # might not need this line
        self.move_group_hand = move_group_hand
        
        eef_link = move_group_hand.get_end_effector_link
        #group_names = robot.get_group_names()
        self.box_name = ""
        self.robot = robot
        self.scene = scene
        self.eef_link = eef_link
        
        
        #group_name_hand = "panda_hand"
        #move_group_hand = moveit_commander.MoveGroupCommander(move_group_hand) 
        
    #this function allows one to specify the revolute joint angles for the robot being controlled.
    def go_to_joint_state(self):
        move_group = self.move_group

        joint_goal = move_group.get_current_joint_values()

        #each joint is given an initial angle that is relative to zero angles in the base configuration of the robot. Tau is established earlier as being 2pi
        joint_goal[0] = 0
        joint_goal[1] = -pi/4
        joint_goal[2] = 0
        joint_goal[3] = -3*pi/4
        joint_goal[4] = 0
        joint_goal[5] = pi/2
        joint_goal[6] = pi/4
        

        move_group.go(joint_goal, wait=True)

        move_group.stop()

        current_joints = move_group.get_current_joint_values()
        return all_close(joint_goal, current_joints, 0.01)

    #this function uses operational space to control the robot, where poses are specified based on the coordinates of the end effector of the UR5e robot

    def go_to_pose_goal(self):
 
        move_group = self.move_group
        
        #the arguments of orientation and the x, y, and z positions are then set to the following functions

        pose_goal = geometry_msgs.msg.Pose()
        pose_goal.orientation.w = 1
        # pose_goal.position.x = posX
        # pose_goal.position.y = posY
        # pose_goal.position.z = posZ
        current_pose = self.move_group.get_current_pose().pose

        move_group.set_pose_target(pose_goal)

        success = move_group.go(wait=True)

        move_group.stop()
        
        move_group.clear_pose_targets()
        current_pose = self.move_group.get_current_pose().pose
        
        return all_close(pose_goal, current_pose, 0.01)

    def plan1(self,x, y):
        
        move_group = self.move_group

        waypoints = []

        wpose = move_group.get_current_pose().pose
        wpose.position.z = zup  
        wpose.position.x= x
        wpose.position.y = y
        waypoints.append(copy.deepcopy(wpose))
        (plan, fraction) = move_group.compute_cartesian_path(
                                        waypoints,   # waypoints to follow
                                        0.01,        # eef_step
                                        0.0)         # jump_threshold
        #return plan, fraction
        move_group.execute(plan, wait=True)

    def plan2(self):
        
        move_group = self.move_group

        waypoints = []

        wpose = move_group.get_current_pose().pose
        wpose.position.z = zdown
        waypoints.append(copy.deepcopy(wpose))
        (plan, fraction) = move_group.compute_cartesian_path(
                                        waypoints,   # waypoints to follow
                                        0.01,        # eef_step
                                        0)         # jump_threshold
        #return plan, fraction
        move_group.execute(plan, wait=True)
    
    def plan3(self,x, y):
        client = actionlib.SimpleActionClient('/franka_gripper/grasp', GraspAction)
        rospy.loginfo("CONNECTING")
        client.wait_for_server()
        action = GraspGoal(width=0.01905,speed=0.08,force=1000000000) #1000000000 for force
        rospy.loginfo("SENDING ACTION")
        client.send_goal(action)
        #client.wait_for_result(rospy.Duration.from_sec(5.0)) --> this line halts process

        move_group = self.move_group

        waypoints = []

        wpose = move_group.get_current_pose().pose
        wpose.position.z = zup
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z = zup
        wpose.position.x= x
        wpose.position.y = y
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z = zdown
        waypoints.append(copy.deepcopy(wpose))
        #move_group.set_planning_time(100)
        (plan, fraction) = move_group.compute_cartesian_path(
                                        waypoints,   # waypoints to follow
                                        0.001,        # eef_step
                                        0)         # jump_threshold
        #plan = self.slow_down(plan)
        #return plan, fraction
        move_group.execute(plan, wait=True)
        rospy.loginfo("DONE")

        

    def plan4(self):
        
        move_group = self.move_group

        waypoints = []

        wpose = move_group.get_current_pose().pose
        wpose.position.z = zup
        waypoints.append(copy.deepcopy(wpose))

        (plan, fraction) = move_group.compute_cartesian_path(
                                        waypoints,   # waypoints to follow
                                        0.01,        # eef_step
                                        0.0)         # jump_threshold
        #return plan, fraction
        move_group.execute(plan, wait=True)

    def close(self):
        move_group_hand = self.move_group_hand

        #open
        finger_joint = move_group_hand.get_current_joint_values()
        finger_joint = [0.01905,0.01905]   
        #finger_joint = [0.018, 0.018]         
        move_group_hand.go(finger_joint, wait = True)
        move_group_hand.stop()
        
    def open(self):
        move_group_hand = self.move_group_hand
        
        #close
        finger_joint = [0.03, 0.03]
        move_group_hand.go(finger_joint, wait = True)
        move_group_hand.stop()

    def grasp(self):
        client = actionlib.SimpleActionClient('/franka_gripper/grasp', GraspAction)
        rospy.loginfo("CONNECTING")
        client.wait_for_server()
        action = GraspGoal(width=0.01905,speed=0.08,force=1500)
        rospy.loginfo("SENDING ACTION")
        client.send_goal(action)
        
        #plan3 
        
        client.wait_for_result(rospy.Duration.from_sec(150.0))
        rospy.loginfo("DONE")
        #Gripper.grip(black_pawn6)
        
    def slow_down(self,traj):

        new_traj = RobotTrajectory()
        new_traj.joint_trajectory = traj.joint_trajectory
        n_joints = len(traj.joint_trajectory.joint_names)
        n_points = len(traj.joint_trajectory.points)

        spd = 0.7 # Lower this when using the Gazebo Robot

        for i in range(n_points):
            new_traj.joint_trajectory.points[i].time_from_start = traj.joint_trajectory.points[i].time_from_start / spd

            # rospy.loginfo(type(traj.joint_trajectory.points[i]))
            v = list(new_traj.joint_trajectory.points[i].velocities)
            a = list(new_traj.joint_trajectory.points[i].accelerations)
            p = list(new_traj.joint_trajectory.points[i].positions)

            for j in range(n_joints):
                # rospy.loginfo(type(new_traj.joint_trajectory.points[i].velocities))
                v[j] = traj.joint_trajectory.points[i].velocities[j] * spd
                a[j] = traj.joint_trajectory.points[i].accelerations[j] * spd**2
                p[j] = traj.joint_trajectory.points[i].positions[j]

                # new_traj.joint_trajectory.points[i].accelerations[j] = traj.joint_trajectory.points[i].accelerations[j] * spd
                # new_traj.joint_trajectory.points[i].positions[j] = traj.joint_trajectory.points[i].positions[j]

            v = tuple(v)
            a = tuple(a)
            p = tuple(p)

            new_traj.joint_trajectory.points[i].velocities = v
            new_traj.joint_trajectory.points[i].accelerations = a
            new_traj.joint_trajectory.points[i].positions = p

            # rospy.loginfo( new_traj.joint_trajectory.points[i].velocities[j])
            # rospy.loginfo( new_traj.joint_trajectory.points[i].accelerations[j])
            # rospy.loginfo( new_traj.joint_trajectory.points[i].positions[j])
            
        return new_traj
    
        
#Function to spawn chess pieces
        
#def capture:
def spawn_object(file_name, number, pos):
    x = pos[0]
    y = pos[1]
    z = pos[2]
    client = rospy.ServiceProxy("/gazebo/spawn_sdf_model", SpawnModel)
    #path = "/home/lmt53/catkin_ws/src/final_dependencies/src/chess_files_cws/" + file_name + ".sdf"
    path = "/home/rr238/catkin_ws/src/ChessSetup/" + file_name + ".sdf"
    client(
        model_name = file_name + str(number),
        model_xml = open(path, "r").read(),
        robot_namespace = "/moveit_commander",
        initial_pose = Pose(position = Point(x, y, z),
        orientation = Quaternion(0, 0, 0, 0)),
        reference_frame = "world"
    )

    
def load_all():
    spawn_object('board', 0, [0.5080, 0, 0])
    spawn_object('white_rook', 1, convert_rowcol_to_xy([7,0]))
    spawn_object('white_knight', 1, convert_rowcol_to_xy([7,1]))
    spawn_object('white_bishop', 1, convert_rowcol_to_xy([7,2]))
    spawn_object('white_queen', 1, convert_rowcol_to_xy([7,3]))
    spawn_object('white_king', 1, convert_rowcol_to_xy([7,4]))
    spawn_object('white_bishop', 2, convert_rowcol_to_xy([7,5]))
    spawn_object('white_knight', 2, convert_rowcol_to_xy([7,6]))
    spawn_object('white_rook', 2, convert_rowcol_to_xy([7,7]))
    spawn_object('white_pawn', 1, convert_rowcol_to_xy([6,0]))
    spawn_object('white_pawn', 2, convert_rowcol_to_xy([6,1]))
    spawn_object('white_pawn', 3, convert_rowcol_to_xy([6,2]))
    spawn_object('white_pawn', 4, convert_rowcol_to_xy([6,3]))
    spawn_object('white_pawn', 5, convert_rowcol_to_xy([6,4]))
    spawn_object('white_pawn', 6, convert_rowcol_to_xy([6,5]))
    spawn_object('white_pawn', 7, convert_rowcol_to_xy([6,6]))
    spawn_object('white_pawn', 8, convert_rowcol_to_xy([6,7]))
    
    spawn_object('black_pawn', 1, convert_rowcol_to_xy([1,0]))
    spawn_object('black_pawn', 2, convert_rowcol_to_xy([1,1]))
    spawn_object('black_pawn', 3, convert_rowcol_to_xy([1,2]))
    spawn_object('black_pawn', 4, convert_rowcol_to_xy([1,3]))
    spawn_object('black_pawn', 5, convert_rowcol_to_xy([1,4]))
    spawn_object('black_pawn', 6, convert_rowcol_to_xy([1,5]))
    spawn_object('black_pawn', 7, convert_rowcol_to_xy([1,6]))
    spawn_object('black_pawn', 8, convert_rowcol_to_xy([1,7]))
    spawn_object('black_rook', 1, convert_rowcol_to_xy([0,0]))
    spawn_object('black_knight', 1, convert_rowcol_to_xy([0,1]))
    spawn_object('black_bishop', 1, convert_rowcol_to_xy([0,2]))
    spawn_object('black_queen', 1, convert_rowcol_to_xy([0,3]))
    spawn_object('black_king', 1, convert_rowcol_to_xy([0,4]))
    spawn_object('black_bishop', 2, convert_rowcol_to_xy([0,5]))
    spawn_object('black_knight', 2, convert_rowcol_to_xy([0,6]))
    spawn_object('black_rook', 2, convert_rowcol_to_xy([0,7]))
       
def main():
    try:
        print("Chess Game")
        print("")
        input(
            "============ Press `Enter` to load chess setup ..."
        )        
        proj = myProject()
        load_all()
        input(
            "============ Press `Enter` to get robot ready ..."
        )        
        #proj.convert_rowcol_to_xy([0,0])
        #proj.convert_rowcol_to_xy([7,7])
        proj.go_to_joint_state()

        #pose goals for letter E utilizing the relevant coordinates
        #proj.go_to_pose_goal(1, .4, .1, .4)
        x,y,z = convert_rowcol_to_xy([1,3])
        proj.plan1(x,y)
        proj.open()
        proj.plan2()
        #proj.close()
        #proj.grasp()
        
        #proj.attach_box()
        x,y,z = convert_rowcol_to_xy([2,7])
        proj.plan3(x,y)
        proj.plan4()

        
        #proj.go_to_pose_goal()
        # proj.plan2()
        # proj.plan3(0.5, 0.5)
        # proj.plan4()

        # proj.go_to_joint_state()

    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return


if __name__ == "__main__":
    main()