#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Nov  5 10:13:20 2022

@author: luistrejo
"""

#inputs
robot_name = "white_checker" #enter model name
color = "White" #enter model color
mass = 10 #kg
x_inches = 2.74 #inches
y_inches = 2.74 #inches
z_inches = 1 #inches

x = x_inches * 0.0254 #meters
y = y_inches * 0.0254 #meters
z = z_inches * 0.0254 #meters

#calculate inertia values
ixx = 1/12 * mass*(z**2 + y**2)
iyy = 1/12 * mass*(x**2 + z**2)
izz = 1/12 * mass*(x**2 + y**2)

#write size and inertia parameters
inertia_statement = "<inertia ixx='{}' ixy='0.0' ixz='0.0' iyy='{}' iyz='0.0' izz='{}'/>".format(ixx, iyy, izz)
size_statement = "<box size='{} {} {}'/>".format(x, y, z)

#write robot name and color
link_name = robot_name + "_link"

#make script
script = """<robot name='{}'>

    <!-- Colours for RVIZ for geometric elements -->
    <material name='Blue'>
        <color rgba='0 0 0.8 1'/>
    </material>
    <material name='Red'>
        <color rgba='0.8 0 0 1'/>
    </material>
    <material name='Green'>
        <color rgba='0 0.8 0 1'/>
    </material>
    <material name='Grey'>
        <color rgba='0.75 0.75 0.75 1'/>
    </material>
    <material name='White'>
        <color rgba='1.0 1.0 1.0 1'/>
    </material>
    <material name='Black'>
        <color rgba='0 0 0 1'/>
    </material>


	<!-- * * * Link Definitions * * * -->

    <link name='{}'>
 	    <inertial>
            <origin xyz='0 0 0' rpy='0 0 0'/>
            <mass value='{}' />
            {}
        </inertial>
        <collision>
            <origin xyz='0 0 0' rpy='0 0 0'/>
            <geometry>
                {}
            </geometry>
        </collision>
        <visual>
            <origin rpy='0.0 0 0' xyz='0 0 0'/>
            <geometry>
                {}
            </geometry>
            <material name='{}'/>
        </visual>
    </link>
    
    <gazebo reference='{}'>
        <kp>100000</kp>
        <kd>1</kd>
        <mu1>30.0</mu1>
        <mu2>30.0</mu2>
        <material>Gazebo/{}</material>
    </gazebo>

</robot>""".format(robot_name, link_name, mass, inertia_statement, size_statement, size_statement, color, link_name, color)

#print(script)
'''
"x" - Create - will create a file, returns an error if the file exist
"a" - Append - will create a file if the specified file does not exist
"w" - Write - will create a file if the specified file does not exist
'''
f = open(robot_name + ".sdf", "x")
f.write(script)
f.close()