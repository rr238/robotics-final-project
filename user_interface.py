import re

class UserInterface:
        
    def squareToCoords(square):
        square_split = [x for x in square]
                
        row = 8 - int(square_split[1])
        col_name = square_split[0]
        
        col = ord(col_name.lower()) - 97
        
        return (row, col)
    
    def coordsToSquare(coords):
        coords_split = [x for x in coords]
        
        row = 8 - coords_split[0] #- 6
        col = chr(coords_split[1] + 97)
        row_col = col + str(row)
        
        return row_col
    
    def promptUserForStartPos():
        current_square = input("On what square is the piece you would like to move now? If you would like to resign, type \'resign\': ")
        
        if current_square.lower()=="resign":
            return -1

        if re.match('^[a-h][1-8]$', current_square):
            start_coords = UserInterface.squareToCoords(current_square)
        
        else:
            print("Sorry! That is not a legal move. Please try again.")
            start_coords = UserInterface.promptUserForStartPos()
        
        return start_coords
            
    def promptUserForEndPos(moves):
        print("Your available moves are: \n")
        for m in moves:
            m_conv = UserInterface.coordsToSquare(m)
            print(f"{m_conv} \n")
            
        target_square = input("To what square would you like to move that piece? If you would like to select a different piece to move, type \'back\': ")
        
        legal = False
        
        if target_square.lower()=="back":
            return -1
        
        for m in moves:
            m = UserInterface.coordsToSquare(m)
            if target_square.lower() == m.lower():
                legal = True
                break
                
        if(legal):
            print("Thank you for your move.")
            end_coords = UserInterface.squareToCoords(target_square)
        
        else:
            print("Sorry! That is not a legal move. Please try again.")
            end_coords = UserInterface.promptUserForEndPos(moves)
        
        return end_coords